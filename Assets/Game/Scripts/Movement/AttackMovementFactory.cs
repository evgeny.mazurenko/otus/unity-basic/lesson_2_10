using Game.Scripts.Weapon;
using UnityEngine;

namespace Game.Scripts.Movement
{
    public static class AttackMovementFactory
    {
        public static AttackMovement CreateAttackMovement(
            Transform fromPosition, AttackMovementParams attackMovementParams, WeaponType weaponType, Animator animator
        )
        {
            switch (weaponType)
            {
                case WeaponType.GUN:
                    return new RangeAttackMovement(fromPosition, attackMovementParams);
                case WeaponType.BAT:
                    return new MeleeAttackMovement(fromPosition, attackMovementParams, animator);
            }

            return new DebugAttackMovement(fromPosition, attackMovementParams);
        }
    }
}