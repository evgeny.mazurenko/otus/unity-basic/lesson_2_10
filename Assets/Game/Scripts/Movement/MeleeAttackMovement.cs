using System.Collections;
using UnityEngine;

namespace Game.Scripts.Movement
{
    public sealed class MeleeAttackMovement : AttackMovement
    {
        private const float MOVEMENT_ERROR = 0.05f;
        private const int IDLE_ANIM_STATE = 0;
        private const int MOVE_ANIM_STATE = 1;
        private static readonly int State = Animator.StringToHash("State");
        
        private Animator _animator;
        
        private SoundEffectController _soundEffects;
        
        public MeleeAttackMovement(Transform objectTransform, AttackMovementParams attackMovementParams, Animator animator)
            : base(objectTransform, attackMovementParams)
        {
            _animator = animator;
            _soundEffects = SoundEffectController.INSTANCE;
        }

        public override IEnumerator MoveToAttackedPosition(Transform attackedTransform)
        {
            yield return base.MoveToAttackedPosition(attackedTransform);
            yield return RotateObjectTo(Quaternion.LookRotation(AttackedDirection));
            yield return MoveObjectTo(
                AttackedDirection, attackedTransform.position, AttackMovementParams.AttackDistance
            );
        }

        public override IEnumerator ReturnToBasePosition()
        {
            yield return MoveObjectTo(-AttackedDirection, BasePosition, 0);
            yield return RotateObjectTo(BaseRotation);
        }

        private IEnumerator MoveObjectTo(Vector3 direction, Vector3 targetPosition, float distance)
        {
            float stepMoving = AttackMovementParams.MovementSpeed * Time.deltaTime;
            _animator.SetInteger(State, MOVE_ANIM_STATE);
            _soundEffects.PlayStepMovementLoop();
            do
            {
                ObjectTransform.position += direction * stepMoving;
                yield return null;
            } 
            while (Vector3.Distance(ObjectTransform.position, targetPosition) > distance + MOVEMENT_ERROR);
            _soundEffects.StopStepMovementLoop();
            _animator.SetInteger(State, IDLE_ANIM_STATE);
        }
    }
}