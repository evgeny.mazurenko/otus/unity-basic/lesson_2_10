using System.Collections;
using UnityEngine;

namespace Game.Scripts.Movement
{
    public sealed class RangeAttackMovement: AttackMovement
    {
        public RangeAttackMovement(Transform objectTransform, AttackMovementParams attackMovementParams) 
            : base(objectTransform, attackMovementParams)
        {
        }
        
        public override IEnumerator MoveToAttackedPosition(Transform attackedTransform)
        {
            yield return base.MoveToAttackedPosition(attackedTransform);
            yield return RotateObjectTo(Quaternion.LookRotation(AttackedDirection));
        }

        public override IEnumerator ReturnToBasePosition()
        {
            yield return RotateObjectTo(BaseRotation);
        }
    }
}