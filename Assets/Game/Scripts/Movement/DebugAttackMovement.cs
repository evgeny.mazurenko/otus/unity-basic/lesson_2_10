using System.Collections;
using UnityEngine;

namespace Game.Scripts.Movement
{
    public class DebugAttackMovement: AttackMovement
    {
        public DebugAttackMovement(Transform objectTransform, AttackMovementParams attackMovementParams) : base(objectTransform, attackMovementParams)
        {
        }

        public override IEnumerator MoveToAttackedPosition(Transform attackedTransform)
        {
            base.MoveToAttackedPosition(attackedTransform);
            Debug.Log($"Attack from position/rotation [{BasePosition}]/[{BaseRotation.eulerAngles}] to [{attackedTransform.position}]/[{attackedTransform.rotation.eulerAngles}]");
            yield return null;
        }

        public override IEnumerator ReturnToBasePosition()
        {
            Debug.Log($"Restore base position/rotation [{BasePosition}]/[{BaseRotation.eulerAngles}]");
            yield return null;
        }
    }
}