using System;
using UnityEngine;

namespace Game.Scripts.Movement
{
    [Serializable]
    public class AttackMovementParams
    {
        [field:SerializeField]
        public float MovementSpeed { get; set; }
        
        [field:SerializeField]
        public float RotationSpeed { get; set; }
        
        [field:SerializeField]
        public float AttackDistance { get; set; }
    }
}