using System.Collections;
using UnityEngine;

namespace Game.Scripts.Movement
{
    public abstract class AttackMovement
    {
        private const float ROTATION_ERROR = 0.00001f;
        protected Transform ObjectTransform;
        protected Vector3 BasePosition;
        protected Quaternion BaseRotation;
        protected AttackMovementParams AttackMovementParams;
        protected Vector3 AttackedDirection;

        protected AttackMovement(Transform objectTransform, AttackMovementParams attackMovementParams)
        {
            ObjectTransform = objectTransform;
            AttackMovementParams = attackMovementParams;
        }

        public virtual IEnumerator MoveToAttackedPosition(Transform attackedTransform)
        {
            SaveBasePosition();
            AttackedDirection = (attackedTransform.position - ObjectTransform.position).normalized;
            yield return null;
        }

        public abstract IEnumerator ReturnToBasePosition();

        private void SaveBasePosition()
        {
            BasePosition = ObjectTransform.position;
            BaseRotation = ObjectTransform.rotation;
        }

        protected IEnumerator RotateObjectTo(Quaternion to)
        {
            float stepRotation = AttackMovementParams.RotationSpeed * Time.deltaTime;
            do
            {
                Quaternion nextRotation = Quaternion.Slerp(ObjectTransform.rotation, to, stepRotation);
                ObjectTransform.rotation = nextRotation;
                yield return null;
            } 
            while (!IsQuaternionApproximately(ObjectTransform.rotation, to));
        }
        
        private bool IsQuaternionApproximately(Quaternion a, Quaternion b)
        {
            return Mathf.Abs(Quaternion.Dot(a, b)) >= 1 - ROTATION_ERROR;
        }
    }
}