using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Scripts.UI.Cursor
{
    public sealed class CursorHandler: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
    {
        private PoliceCursor _policeCursor;

        private void Start()
        {
            _policeCursor = PoliceCursor.INSTANCE;
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            _policeCursor?.SetPointerCursor();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _policeCursor?.SetDefaultCursor();
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            _policeCursor?.SetClickCursor();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _policeCursor?.SetPointerCursor();
        }
    }
}