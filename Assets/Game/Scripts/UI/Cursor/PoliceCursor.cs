using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class PoliceCursor : MonoBehaviour
{
    public static PoliceCursor INSTANCE { get; private set; }

    [SerializeField]
    private Texture2D cursorDefault;
    
    [SerializeField]
    private Texture2D cursorPointer;
    
    [SerializeField]
    private Texture2D cursorClick;

    private AudioSource _audioSource;

    private void Awake()
    {
        if (INSTANCE != null)
        {
            Destroy(gameObject);
            return;
        }

        INSTANCE = this;
        _audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
        SetDefaultCursor();
    }

    public void SetDefaultCursor()
    {
        Cursor.SetCursor(cursorDefault, Vector3.zero, CursorMode.Auto);
    }

    public void SetPointerCursor()
    {
        Cursor.SetCursor(cursorPointer, Vector3.zero, CursorMode.Auto);
    }
    
    public void SetClickCursor()
    {
        _audioSource.Play();
        Cursor.SetCursor(cursorClick, Vector3.zero, CursorMode.Auto);
    }
}
