using System;
using UnityEngine;

namespace Game.Scripts.UI
{
    public class MenuCanvas: MonoBehaviour
    {
        [SerializeField] 
        private MenuType menuType;

        private CanvasGroup _canvasGroup;

        public CanvasGroup CanvasGroup => _canvasGroup;

        public MenuType MenuType => menuType;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }
    }
}