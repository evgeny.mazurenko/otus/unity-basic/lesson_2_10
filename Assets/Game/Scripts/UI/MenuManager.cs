using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Game.Scripts.UI
{
    public class MenuManager: MonoBehaviour
    {
        [FormerlySerializedAs("menuCanvas")] [SerializeField]
        private MenuCanvas[] menuCanvases;

        [SerializeField]
        private Music music;

        private void OnEnable()
        {
            music.PlayMusic();
        }

        private void OnDisable()
        {
            music.StopMusic();
        }

        private void Start()
        {
            ActivateMenu(MenuType.MAIN);
        }

        #region Button Handlers
        
        public void ShowChangeLevelMenu()
        {
            ActivateMenu(MenuType.CHANGE_LEVEL);
        }
        
        public void StartLevel1()
        {
            StartLevel("Level_1");
        }
        
        public void StartLevel2()
        {
            StartLevel("Level_2");
        }

        public void ShowSettingsMenu()
        {
            ActivateMenu(MenuType.SETTINGS);
        }

        public void BackToMainMenu()
        {
            ActivateMenu(MenuType.MAIN);
        }

        public void ExitGame()
        {
            Debug.Log("Exit");
            //Application.Quit();
        }

        private void StartLevel(string name)
        {
            ActivateMenu(MenuType.NONE);
            music.StopMusic();
            SceneLoader.INSTANCE.LoadScene(name);
        }
        
        #endregion

        private void ActivateMenu(MenuType menuType)
        {
            foreach (var menuCanvas in menuCanvases)
            {
                UIUtils.SetEnableCanvasGroup(menuCanvas.CanvasGroup, menuCanvas.MenuType == menuType);
            }
        }
    }
}