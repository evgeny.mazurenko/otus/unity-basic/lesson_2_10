namespace Game.Scripts.UI
{
    public enum MenuType
    {
        NONE = 0,
        MAIN = 1,
        SETTINGS = 2,
        CHANGE_LEVEL = 3,
        LOADING_SCREEN = 4
    }
}