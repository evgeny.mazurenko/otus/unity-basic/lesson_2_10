namespace Game.Scripts.UI.UIStates
{
    public sealed class MenuUIState: UIState
    {
        public MenuUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(true);
        }
    }
}