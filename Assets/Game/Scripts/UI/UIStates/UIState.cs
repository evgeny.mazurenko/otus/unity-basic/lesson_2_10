namespace Game.Scripts.UI.UIStates
{
    public abstract class UIState
    {
        protected readonly UIController _uiController;

        protected UIState(UIController uiController)
        {
            _uiController = uiController;
        }

        public abstract void UpdateUI();

        protected void SetActiveMenu(bool enable)
        {
            _uiController.menuButton.interactable = !enable;
            UIUtils.SetEnableCanvasGroup(_uiController.menuGroup, enable);
            UIUtils.SetEnableCanvasGroup(_uiController.gameControlsGroup, !enable);
        }
    }
}