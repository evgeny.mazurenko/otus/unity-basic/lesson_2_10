namespace Game.Scripts.UI.UIStates
{
    public sealed class BaseUIState: UIState
    {
        public BaseUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(false);
            _uiController.continueButton.interactable = true;
            _uiController.confirmButton.interactable = true;
            _uiController.selectEnemyButton.interactable = true;
            _uiController.gameWonLabel.text = "";
            _uiController.gameLostLabel.text = "";
        }
    }
}