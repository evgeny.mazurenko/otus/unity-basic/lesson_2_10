namespace Game.Scripts.UI.UIStates
{
    public sealed class GameWonUIState: UIState
    {
        public GameWonUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(false);
            UIUtils.SetEnableCanvasGroup(_uiController.gameControlsGroup, false);
            _uiController.gameWonLabel.text = "Вы одержали победу!";
        }
    }
}