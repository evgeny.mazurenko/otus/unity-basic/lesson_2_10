namespace Game.Scripts.UI.UIStates
{
    public sealed class GameTurnOffUIState: UIState
    {
        public GameTurnOffUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(false);
            _uiController.confirmButton.interactable = false;
            _uiController.selectEnemyButton.interactable = false;
        }
    }
}