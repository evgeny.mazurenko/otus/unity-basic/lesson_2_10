namespace Game.Scripts.UI.UIStates
{
    public class UnlockSniperCallUIState: UIState
    {
        public UnlockSniperCallUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(false);
            _uiController.callSniperButton.interactable = true;
        }
    }
}