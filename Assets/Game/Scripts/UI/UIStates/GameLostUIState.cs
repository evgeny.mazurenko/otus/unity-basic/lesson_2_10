namespace Game.Scripts.UI.UIStates
{
    public sealed class GameLostUIState: UIState
    {
        public GameLostUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(false);
            UIUtils.SetEnableCanvasGroup(_uiController.gameControlsGroup, false);
            _uiController.gameLostLabel.text = "Вы проиграли";
        }
    }
}