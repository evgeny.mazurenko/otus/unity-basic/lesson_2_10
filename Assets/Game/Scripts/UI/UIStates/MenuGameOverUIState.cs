namespace Game.Scripts.UI.UIStates
{
    public sealed class MenuGameOverUIState: UIState
    {
        public MenuGameOverUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(true);
            _uiController.continueButton.interactable = false;
        }
    }
}