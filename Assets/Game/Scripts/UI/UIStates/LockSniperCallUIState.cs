namespace Game.Scripts.UI.UIStates
{
    public class LockSniperCallUIState: UIState
    {
        public LockSniperCallUIState(UIController uiController) : base(uiController)
        {
        }

        public override void UpdateUI()
        {
            SetActiveMenu(false);
            _uiController.callSniperButton.interactable = false;
        }
    }
}