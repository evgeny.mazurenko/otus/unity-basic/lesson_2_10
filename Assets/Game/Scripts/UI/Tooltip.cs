using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Scripts.UI
{
    public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private TextMeshProUGUI label;

        [SerializeField]
        private string text;
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            label.text = text;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            label.text = "";
        }
    }
}