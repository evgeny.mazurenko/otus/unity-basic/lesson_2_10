using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public sealed class SceneLoader: MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup canvasGroup;

        [SerializeField]
        private Image loadingBar;
        
        public static SceneLoader INSTANCE { get; private set; }

        private void Awake()
        {
            // Защита от повторного создания родительского GameObject: уничтожение 
            // текущего объекта при условии инстанцирования подобного экземпляра ранее
            if (INSTANCE != null)
            {
                Destroy(gameObject);
                return;
            }
            
            INSTANCE = this;
            UIUtils.SetEnableCanvasGroup(canvasGroup, false);
            DontDestroyOnLoad(gameObject);
        }

        public void LoadScene(string sceneName)
        {
            StartCoroutine(LoadSceneAsync(sceneName));
        }

        private IEnumerator LoadSceneAsync(string sceneName)
        {
            UIUtils.SetEnableCanvasGroup(canvasGroup, true);
            AsyncOperation loading = SceneManager.LoadSceneAsync(sceneName);
            while (!loading.isDone)
            {
                loadingBar.fillAmount = loading.progress;
                yield return null;
            }
            //Добивка до 100%, так как из цикла выход происходит ранее
            loadingBar.fillAmount = loading.progress;
            
            UIUtils.SetEnableCanvasGroup(canvasGroup, false);
        }
    }
}