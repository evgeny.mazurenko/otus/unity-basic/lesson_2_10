using UnityEngine;

namespace Game.Scripts.UI
{
    public static class UIUtils
    {
        public static void SetEnableCanvasGroup(CanvasGroup canvasGroup, bool enable)
        {
            canvasGroup.alpha = enable ? 1f : 0f;
            canvasGroup.interactable = enable;
            canvasGroup.blocksRaycasts = enable;
        }
    }
}