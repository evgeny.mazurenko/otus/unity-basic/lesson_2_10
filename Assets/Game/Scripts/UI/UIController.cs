using System;
using Game.Scripts.UI.UIStates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    [Serializable]
    public sealed class UIController
    {
        #region UI Controls
        
        public Button menuButton;
        
        [Header("MenuItems")]
        public CanvasGroup menuGroup;
        public Button continueButton;
        
        [Header("GameControls")]
        public CanvasGroup gameControlsGroup;
        public Button confirmButton;
        public Button selectEnemyButton;
        public Button callSniperButton;
        
        [Header("EndGameControls")] 
        public TextMeshProUGUI gameWonLabel;
        public TextMeshProUGUI gameLostLabel;
        
        #endregion
            
        
        #region Init UIStates

        private readonly UIState GameState;
        private readonly UIState GameTurnOffState;
        private readonly UIState MenuState;
        private readonly UIState MenuGameOverState;
        private readonly UIState GameLostState;
        private readonly UIState GameWonState;
        private readonly UIState LockSniperCallState;
        private readonly UIState UnockSniperCallState;
        
        public UIController()
        {
            GameState = new BaseUIState(this);
            GameTurnOffState = new GameTurnOffUIState(this);
            LockSniperCallState = new LockSniperCallUIState(this);
            UnockSniperCallState = new UnlockSniperCallUIState(this);
            MenuState = new MenuUIState(this);
            MenuGameOverState = new MenuGameOverUIState(this);
            GameWonState = new GameWonUIState(this);
            GameLostState = new GameLostUIState(this);
        }
        
        #endregion

        private UIState _menuState;
        private UIState _continueState;
        private UIState _currentState;
        
        public void ShowMenu()
        {
            _continueState = _currentState;
            SetState(_menuState);
        }

        public void TurnControlsOff()
        {
            SetState(GameTurnOffState);
        }

        public void Continue()
        {
            SetState(_continueState);
        }

        public void ShowGameControls()
        {
            _menuState = MenuState;
            SetState(GameState);
        }

        public void ShowGameWonLabel()
        {
            _menuState = MenuGameOverState;
            SetState(GameWonState);
        }
        
        public void ShowGameLostLabel()
        {
            _menuState = MenuGameOverState;
            SetState(GameLostState);
        }

        public void LockCallSniperButton()
        {
            SetState(LockSniperCallState);
        }
        
        public void UnlockCallSniperButton()
        {
            SetState(UnockSniperCallState);
        }

        private void SetState(UIState state)
        {
            state.UpdateUI();
            _currentState = state;
        }
    }
}