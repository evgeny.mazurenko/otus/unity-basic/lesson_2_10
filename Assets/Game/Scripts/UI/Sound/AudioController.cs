using System;
using UnityEngine;
using UnityEngine.Audio;

namespace Game.Scripts.UI.Sound
{
    public sealed class AudioController: MonoBehaviour
    {
        [SerializeField]
        private AudioMixer audioMixer;

        [SerializeField]
        private MixerTuner[] tuners;

        private void OnEnable()
        {
            ActionForeachTuner(tuner => tuner.SubscribeOnValueChange(audioMixer));
        }
        
        private void OnDisable()
        {
            ActionForeachTuner(tuner => tuner.UnsubscribeOnValueChange(audioMixer));
        }

        private void Start()
        {
            ActionForeachTuner(tuner => tuner.InitSlider(audioMixer));
        }

        private void ActionForeachTuner(Action<MixerTuner> tunerAction)
        {
            foreach (var tuner in tuners)
            {
                tunerAction(tuner);
            }
        }
    }
}