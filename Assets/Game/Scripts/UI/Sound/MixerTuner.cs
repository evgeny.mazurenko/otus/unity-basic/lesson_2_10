using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Game.Scripts.UI.Sound
{
    [Serializable]
    public sealed class MixerTuner
    {
        [SerializeField] 
        private string groupName;
        
        [SerializeField]
        private Slider slider;

        public void InitSlider(AudioMixer mixer)
        {
            if (mixer.GetFloat(groupName, out float mixerValue))
                slider.value = mixerValue;
        }

        public void SubscribeOnValueChange(AudioMixer mixer)
        {
            void UpdateMixerValue(float value) => mixer.SetFloat(groupName, value);
            slider.onValueChanged.AddListener(UpdateMixerValue);
        }
        
        public void UnsubscribeOnValueChange(AudioMixer mixer)
        {
            void UpdateMixerValue(float value) => mixer.SetFloat(groupName, value);
            slider.onValueChanged.RemoveListener(UpdateMixerValue);
        }
    }
}