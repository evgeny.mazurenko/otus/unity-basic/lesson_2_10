using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class HPBarController: MonoBehaviour
    {
        [SerializeField] 
        private GameObject hpBar;
        
        private CanvasGroup _hpCanvasGroup;
        
        private TextMeshProUGUI _hpPercent;
        
        private Image _hpBarImage;

        private void Start()
        {
            _hpCanvasGroup = GetComponentInChildren<CanvasGroup>();
            _hpBarImage = GetComponentInChildren<Image>();
            _hpPercent = GetComponentInChildren<TextMeshProUGUI>();
            hpBar.transform.LookAt(Camera.main.transform);
        }

        public void UpdateHP(float hpShare)
        {
            if (hpShare > 0)
            {
                _hpBarImage.color = new Color(1 - hpShare, hpShare, 0.2f, 0.7f);
                _hpPercent.text = (hpShare * 100) + "%";
            }
            else
            {
                UIUtils.SetEnableCanvasGroup(_hpCanvasGroup, false);
            }
        }
    }
}