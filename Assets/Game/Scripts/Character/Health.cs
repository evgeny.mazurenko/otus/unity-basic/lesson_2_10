using System;
using UnityEngine;

namespace Game.Scripts.Character
{
    [Serializable]
    public class Health
    {
        public event Action OnDeath;

        public event Action<float> OnChangeHealth;

        [SerializeField]
        private int max;
        
        private int _current = int.MinValue;

        private int CurrentHealth
        {
            get => _current;
            set
            {
                _current = value;
                OnChangeHealth?.Invoke((float)_current / max);
            }
        }

        public bool IsAlive
        {
            get
            {
                InitCurrentHealth();
                return CurrentHealth > 0;
            }
        }

        public void TakeDamage(int damage)
        {
            InitCurrentHealth();
            
            if (CurrentHealth <= 0)
            {
                return;
            }

            int decreasedHealth = CurrentHealth - damage;

            CurrentHealth = decreasedHealth < 0 ? 0 : decreasedHealth;

            if (CurrentHealth <= 0)
            {
                Die();
            }
        }

        private void InitCurrentHealth()
        {
            if (CurrentHealth == int.MinValue)
            {
                CurrentHealth = max;
            }
        }

        private void Die()
        {
            OnDeath?.Invoke();
        }
    }
}