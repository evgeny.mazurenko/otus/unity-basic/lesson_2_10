using UnityEngine;

public sealed class Player : MonoBehaviour
{
    private static readonly int State = Animator.StringToHash("State");
    private static readonly int Shoot = Animator.StringToHash("Shoot");
    private static readonly int Death = Animator.StringToHash("Death");
    private const int IDLE_ANIM_STATE = 0;
    private const int MOVE_ANIM_STATE = 1;

    public int AttackCount { get; private set; }

    [SerializeField] private float moveSpeed;

    [SerializeField] private float rotationSpeed;

    [SerializeField] private Animator animator;

    private Vector3 moveDirection;
    private bool isMoving;
    private bool isAlive = true;

    private WeaponController _weaponController;

    void Start()
    {
        _weaponController = gameObject.GetComponentInChildren<WeaponController>();
    }

    public void Move(Vector3 direction)
    {
        if (isAlive)
        {
            moveDirection = direction;
        }
    }

    public void Attack()
    {
        //Стрельба на бегу выглядит не очень из-за статичных ног, поэтому такую возможность блокирую
        if (!isMoving && isAlive)
        {
            animator.SetTrigger(Shoot);
            AttackCount++;
            _weaponController?.ShowAttackEffect();
        }
    }

    public void Die()
    {
        if (isAlive)
        {
            animator.SetTrigger(Death);
            isAlive = false;
        }
    }

    private void Update()
    {
        if (moveDirection.magnitude > 0)
        {
            UpdateMovement();
            UpdateRotation();
            animator.SetInteger(State, MOVE_ANIM_STATE);
            isMoving = true;
        }
        else
        {
            animator.SetInteger(State, IDLE_ANIM_STATE);
            isMoving = false;
        }
    }

    private void UpdateMovement()
    {
        transform.position += moveDirection * moveSpeed * Time.deltaTime;
    }

    private void UpdateRotation()
    {
        var targetRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
        var nextRotation = Quaternion.Slerp(
            transform.rotation, targetRotation, rotationSpeed * Time.deltaTime
        );
        transform.rotation = nextRotation;
    }
}