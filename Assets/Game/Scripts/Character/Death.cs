using System;
using UnityEngine;


namespace Game.Scripts.Character
{
    /*
     * Класс-обработчик события анимации смерти.
     * Запускает legacy-анимацию исчезновения тела 
     */
    public sealed class Death : MonoBehaviour
    {
        private Burial _burial;

        private void Start()
        {
            _burial = GetComponentInParent<Burial>();
        }

        public void HideBody()
        {
            _burial.Bury();
        }
    }
}