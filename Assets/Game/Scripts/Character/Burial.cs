using UnityEngine;

namespace Game.Scripts.Character
{
    public sealed class Burial : MonoBehaviour
    {
        private Animation _animation;

        private void Start()
        {
            _animation = GetComponent<Animation>();    
        }
        
        public void Bury()
        {
            _animation.Play();
        }
    }
}
