using System.Collections;
using Game.Scripts;
using Game.Scripts.Character;
using Game.Scripts.Effect;
using Game.Scripts.Movement;
using Game.Scripts.UI;
using Game.Scripts.Weapon;
using UnityEngine;

public sealed class Character : MonoBehaviour
{
    private static readonly int AnimationDeathId = Animator.StringToHash("Death");
    
    [SerializeField] 
    private Animator animator;
    
    [SerializeField]
    private Health health;

    [SerializeField] 
    private AttackMovementParams attackMovementParams;
    
    private SoundEffectController _soundEffects;
    
    private HitEffect _hitEffect;

    private WeaponController _weaponController;

    private HPBarController _hpBarController;

    private Weapon _weapon;

    private AttackMovement _attackMovement;
    
    public bool IsAlive => health.IsAlive;

    private void Start()
    {
        _weaponController = GetComponentInChildren<WeaponController>();
        _hpBarController = GetComponentInChildren<HPBarController>();
        _hitEffect = GetComponentInChildren<HitEffect>();
        _weapon = _weaponController.Weapon;
        _attackMovement = AttackMovementFactory.CreateAttackMovement(transform, attackMovementParams, _weapon.Type, animator);
        _soundEffects = SoundEffectController.INSTANCE;

        health.OnDeath += OnDeathHandler;
        health.OnChangeHealth += _hpBarController.UpdateHP;
    }

    private void OnDestroy()
    {
        health.OnDeath -= OnDeathHandler;
        health.OnChangeHealth -= _hpBarController.UpdateHP;
    }

    public IEnumerator Attack(Character attackedCharacter)
    {
        Debug.Log($"Character {name} attack {attackedCharacter.name}");
        
        yield return _attackMovement.MoveToAttackedPosition(attackedCharacter.transform);
        yield return new WaitForSeconds(0.3f);
        
        attackedCharacter.TakeDamage(_weapon.Damage);
        animator.SetTrigger(AnimationIdForWeaponType.GetAnimationId(_weapon.Type));
        _weaponController?.ShowAttackEffect();
        
        yield return new WaitForSeconds(0.5f);
        yield return _attackMovement.ReturnToBasePosition();
    }
    

    public void TakeDamage(int damage)
    {
        health.TakeDamage(damage);
        _hitEffect?.ShowHitEffect();
        if (health.IsAlive)
        {
            _soundEffects.PlayTakeDamage();
        }
    }

    private void OnDeathHandler()
    {
        Debug.Log($"Character {name} is dead");
        _soundEffects.PlayDie();
        animator.SetTrigger(AnimationDeathId);
    }
}
