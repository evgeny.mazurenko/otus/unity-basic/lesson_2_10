using System.Collections;
using UnityEngine;

namespace Game.Scripts.Effect
{
    public sealed class BulletFireAnimated: MonoBehaviour
    {
        private Animation _animation;
        
        private SoundEffectController _soundEffects;

        private void Start()
        {
            _animation = GetComponent<Animation>();
            _soundEffects = SoundEffectController.INSTANCE;
        }

        public IEnumerator Fade()
        {
            if (_animation != null)
            {
                Debug.Log(_soundEffects);
                _soundEffects.PlayShootAttack();
                _animation.Play();
                yield return new WaitForSeconds(_animation.clip.length);
            }
        }
    }
}