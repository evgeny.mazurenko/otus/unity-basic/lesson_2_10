using System;
using UnityEngine;

namespace Game.Scripts.Effect
{
    public sealed class HitEffect: MonoBehaviour
    {
        private ParticleSystem[] _effects;

        private void Start()
        {
            _effects = GetComponentsInChildren<ParticleSystem>();
        }

        public void ShowHitEffect()
        {
            foreach (var effect in _effects)
            {
                effect.Play();
            }
        }
    }
}