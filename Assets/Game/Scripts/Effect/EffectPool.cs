using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.Effect
{
    public sealed class EffectPool<T> where T: MonoBehaviour
    {
        private readonly Queue<T> _pool;
        private readonly T _effectPrefab;

        public EffectPool(T effectPrefab, int preInitCount)
        {
            _effectPrefab = effectPrefab;
            _pool = new Queue<T>(preInitCount);

            for (int i = 0; i < preInitCount; i++)
            {
                T effect = GameObject.Instantiate(_effectPrefab);
                Return(effect);
            }
        }

        public void Return(T effect)
        {
            effect.gameObject.SetActive(false);
            _pool.Enqueue(effect);
        }

        public T Get()
        {
            T effect = _pool.Count > 0 ? _pool.Dequeue() : GameObject.Instantiate(_effectPrefab);
            effect.gameObject.SetActive(true);
            return effect;
        }
    }
}