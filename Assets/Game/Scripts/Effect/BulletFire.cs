using System;
using System.Collections;
using UnityEngine;

namespace Game.Scripts.Effect
{
    public sealed class BulletFire : MonoBehaviour
    {
        [SerializeField] 
        private float lifetime = 0.1f;

        private MeshRenderer[] _renderers;

        private void OnEnable()
        {
            _renderers = GetComponentsInChildren<MeshRenderer>();
        }
        
        //Угасание вспышки выстрела через уменьшение альфа-канала на протяжении времени жизни с последующим унижтожением объекта.
        public IEnumerator Fade()
        {
            float startTime = Time.time;
            if (_renderers != null && _renderers.Length > 0)
            {
                while (Time.time - startTime < lifetime)
                {
                    float deltaTime = Time.time - startTime;
                    foreach (var renderer in _renderers)
                    {
                        var material = renderer.material;
                        Color materialColor = material.color;
                        materialColor.a = 1 - deltaTime / lifetime;
                        material.color = materialColor;
                    }
                    yield return null;
                }
            }

            RestoreParams();
        }

        private void RestoreParams()
        {
            if (_renderers != null && _renderers.Length > 0)
            {
                foreach (var renderer in _renderers)
                {
                    var material = renderer.material;
                    Color materialColor = material.color;
                    materialColor.a = 1;
                    material.color = materialColor;
                }
            }
        }
    }
}