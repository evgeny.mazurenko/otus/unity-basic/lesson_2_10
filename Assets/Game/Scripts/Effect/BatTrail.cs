using System.Collections;
using UnityEngine;

namespace Game.Scripts.Effect
{
    public class BatTrail: MonoBehaviour
    {
        [SerializeField] 
        private float lifetime = 1f;

        private MeshRenderer _renderer;
        
        private SoundEffectController _soundEffects;
        
        private void OnEnable()
        {
            _renderer = GetComponentInChildren<MeshRenderer>();
            _soundEffects = SoundEffectController.INSTANCE;
        }

        public IEnumerator Wing()
        {
            float startTime = Time.time;
            Color materialColor = _renderer.material.color;
            
            while (Time.time - startTime < lifetime)
            {
                materialColor.a = 1 - (Time.time - startTime) / lifetime;
                _renderer.material.color = materialColor;
                yield return null;
            }
            //Звук удара получится несогласованным с анимацией, но оставляю так
            _soundEffects.PlayMeleeAttack();

            materialColor.a = 1;
            _renderer.material.color = materialColor;
        }
    }
}