using Game.Scripts.Effect;
using UnityEngine;

namespace Game.Scripts.Weapon
{
    /*
     * Класс-обработчик события анимации выстрела.
     * Запускает legacy-анимацию воспроизведения вспышки 
     */
    public sealed class Shoot : MonoBehaviour
    {
        private BulletFireAnimated _bulletFire;
    
        void Start()
        {
            _bulletFire = GetComponentInChildren<BulletFireAnimated>();
        }

        public void Fire()
        {
            StartCoroutine(_bulletFire.Fade());
        }
    }
}
