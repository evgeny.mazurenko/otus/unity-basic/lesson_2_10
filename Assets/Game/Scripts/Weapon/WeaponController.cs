using Game.Scripts.Weapon;
using UnityEngine;

public sealed class WeaponController : MonoBehaviour
{
    [SerializeField] 
    private GameObject weaponGameObject;

    [SerializeField] 
    private GameObject weaponEffectPrefab;

    [SerializeField] 
    private Weapon weapon;

    [SerializeField] 
    private float effectDelay = 0.15f;

    [SerializeField] 
    private int effectsCount = 3;

    private IWeaponEffectable _weaponEffectable;

    public GameObject WeaponGameObject => weaponGameObject;

    public GameObject WeaponEffectPrefab => weaponEffectPrefab;

    public float EffectDelay => effectDelay;

    public int EffectsCount => effectsCount;

    public Weapon Weapon => weapon;

    void Awake()
    {
        _weaponEffectable = WeaponEffectableFactory.CreateWeaponEffectable(this, weapon.Type);
    }

    public void ShowAttackEffect()
    {
        _weaponEffectable.ShowAttackEffect();
    }
}