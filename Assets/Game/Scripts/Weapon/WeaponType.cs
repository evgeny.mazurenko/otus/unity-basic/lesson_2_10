namespace Game.Scripts.Weapon
{
    public enum WeaponType
    {
        NONE,
        GUN,
        BAT,
        SNIPER_RIFLE
    }
}