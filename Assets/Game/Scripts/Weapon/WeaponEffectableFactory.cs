namespace Game.Scripts.Weapon
{
    public static class WeaponEffectableFactory
    {
        public static IWeaponEffectable CreateWeaponEffectable(WeaponController weaponController, WeaponType weaponType)
        {
            switch (weaponType)
            {
                case WeaponType.GUN:
                    // Устарело. См. комментарий к классу GunWeaponEffectable
                    //return new GunWeaponEffectable(weaponController);
                    return new DebugWeaponEffectable(weaponController.WeaponGameObject.name);
                case WeaponType.BAT:
                    return new BatWeaponEffectable(weaponController);
            }
            return new DebugWeaponEffectable(weaponController.WeaponGameObject.name);
        }
    }
}