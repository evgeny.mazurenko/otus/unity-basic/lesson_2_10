namespace Game.Scripts.Weapon
{
    public interface IWeaponEffectable
    {
        public void ShowAttackEffect();
    }
}