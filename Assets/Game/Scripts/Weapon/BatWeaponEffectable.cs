using System.Collections;
using System.Collections.Generic;
using Game.Scripts.Effect;
using UnityEngine;

namespace Game.Scripts.Weapon
{
    public sealed class BatWeaponEffectable: IWeaponEffectable
    {
        private const int BAT_TRAIL_POOL_INIT_SIZE = 3;

        private EffectPool<BatTrail> _effectPool;

        private WeaponController _weaponController;
        
        public BatWeaponEffectable(WeaponController weaponController)
        {
            _weaponController = weaponController;
            _effectPool = new EffectPool<BatTrail>(
                weaponController.WeaponEffectPrefab.GetComponent<BatTrail>(),
                BAT_TRAIL_POOL_INIT_SIZE
            );
        }

        public void ShowAttackEffect() {
            _weaponController.StartCoroutine(Wave());
        }

        private IEnumerator Wave()
        {
            yield return new WaitForSeconds(_weaponController.EffectDelay);
            
            for (int i = 0; i < _weaponController.EffectsCount; i++)
            {
                Transform batTransform = _weaponController.WeaponGameObject.transform;
                BatTrail batTrail = _effectPool.Get();
                
                batTrail.transform.rotation = batTransform.rotation;
                batTrail.transform.position = batTransform.position;
                
                yield return batTrail.Wing();
                _effectPool.Return(batTrail);
            }
        }
    }
}