using System.Collections;
using Game.Scripts.Effect;
using UnityEngine;

namespace Game.Scripts.Weapon
{
    /**
     * Приминение эффекта для пистолета - выстрела.
     *
     * Устаревший класс. Теперь вспышка выстрела воспроизводится через Legacy-анимацию объекте BulletFire
     * по событию второго ключевого кадра анимации Shoot.
    */
    public sealed class GunWeaponEffectable : IWeaponEffectable
    {
        //Смещение дула пистолета относительно рукояти
        private static readonly Vector3 MOUTH_POSITION = new Vector3(-0.05f, 0, 0.5f);

        private const int BULLET_FIRE_POOL_INIT_SIZE = 1;

        private EffectPool<BulletFire> _effectPool;

        private WeaponController _weaponController;

        public GunWeaponEffectable(WeaponController weaponController)
        {
            _weaponController = weaponController;
            _effectPool = new EffectPool<BulletFire>(
                weaponController.WeaponEffectPrefab.GetComponent<BulletFire>(),
                BULLET_FIRE_POOL_INIT_SIZE
            );
        }

        public void ShowAttackEffect()
        {
            _weaponController.StartCoroutine(Fire());
        }

        private IEnumerator Fire()
        {
            Transform gunTransform = _weaponController.WeaponGameObject.transform;
            
            yield return new WaitForSeconds(_weaponController.EffectDelay);
            BulletFire bulletFire = _effectPool.Get();
            
            //Привязка положения вспышки к стволу
            bulletFire.transform.rotation = gunTransform.rotation;
            //Приращение локальной позиции поправкой на ствол и перевод в глобальные координаты
            bulletFire.transform.position = gunTransform.TransformPoint(gunTransform.localPosition + MOUTH_POSITION);
            
            yield return bulletFire.Fade();
            _effectPool.Return(bulletFire);
        }
    }
}