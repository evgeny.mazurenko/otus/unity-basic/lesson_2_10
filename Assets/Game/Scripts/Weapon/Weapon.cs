using System;
using UnityEngine;

namespace Game.Scripts.Weapon
{
    [Serializable]
    public class Weapon
    {
        [SerializeField]
        private WeaponType type;
        
        [SerializeField]
        private int damage;

        public WeaponType Type => type;

        public int Damage => damage;
    }
}