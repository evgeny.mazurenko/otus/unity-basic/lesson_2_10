using UnityEngine;

namespace Game.Scripts.Weapon
{
    /**
     * Отладочный вызов применения эффекта оружия
     */
    public sealed class DebugWeaponEffectable: IWeaponEffectable
    {
        private string _weaponName;

        public DebugWeaponEffectable(string weaponName)
        {
            _weaponName = weaponName;
        }
        
        public void ShowAttackEffect()
        {
            //Заглушка для сохранения интерфейса
        }
    }
}