using System;
using System.Collections;
using UnityEngine;

namespace Game.Scripts.Sound
{
    [Serializable]
    public sealed class SoundEffectScope
    {
        [SerializeField]
        private AudioSource audioSource;
        
        [SerializeField]
        private NamedAudioClip[] audioClips;
        
        
        public void PlayNamedAudioClip(string name, bool isLoop = false)
        {
            AudioClip audioClip = GetAudioClipByName(name);
        
            if (audioClip != null)
            {
                audioSource.clip = audioClip;
                if (isLoop)
                {
                    audioSource.loop = isLoop;
                }
                audioSource.Play();
            }
        }
        
        public IEnumerator PlayEffectWithDelay(string name, float delay, bool isLoop = false)
        {
            yield return new WaitForSeconds(delay);
            PlayNamedAudioClip(name, isLoop);
        }

        public void StopCurrentAudioClip()
        {
            audioSource.loop = false;
            audioSource.Stop();
        }

        private AudioClip GetAudioClipByName(string name)
        {
            foreach (var namedAudioClip in audioClips)
            {
                if (name.Equals(namedAudioClip.Name))
                {
                    return namedAudioClip.AudioClip;
                }
            }

            return null;
        }
    }
}