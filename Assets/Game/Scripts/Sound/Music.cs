using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Music : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] tracks;
    
    [SerializeField]
    private AudioSource audioSource;

    private bool _isStopped;

    private void Awake()
    {
        //Не успевает проинициализироваться так как GameController.OnEnable вызывается ранее
        //audioSource = GetComponent<AudioSource>();
    }

    public void PlayMusic()
    {
        _isStopped = false;
        StartCoroutine(PlayTracks());
    }

    public void StopMusic()
    {
        _isStopped = true;
    }

    private IEnumerator PlayTracks()
    {
        while(true)
        {
            AudioClip track = tracks[Random.Range(0, tracks.Length)];
            audioSource.clip = track;
            audioSource.Play();
            while (audioSource.isPlaying)
            {
                yield return null;
                if (_isStopped)
                {
                    yield break;
                }
            }
        }
    }
}
