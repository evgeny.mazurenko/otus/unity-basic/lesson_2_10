using System;
using UnityEngine;

namespace Game.Scripts.Sound
{
    [Serializable]
    public sealed class NamedAudioClip
    {
        [field:SerializeField]
        public string Name { get; set; }
        
        [field:SerializeField]
        public AudioClip AudioClip { get; set; }
    }
}