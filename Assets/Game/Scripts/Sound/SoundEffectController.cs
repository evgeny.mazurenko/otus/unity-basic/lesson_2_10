using System;
using System.Collections;
using System.Collections.Generic;
using Game.Scripts.Sound;
using UnityEngine;

public sealed class SoundEffectController : MonoBehaviour
{
    public static SoundEffectController INSTANCE;
    
    [SerializeField]
    private SoundEffectScope uiSound;
    
    [SerializeField]
    private SoundEffectScope healthSound;

    [SerializeField]
    private SoundEffectScope battleSound;

    private void Awake()
    {
        //Так как не требуется глобальный объект на все сцены не делаю проверку на уже инициалищированный подобный объект,
        //полагаясь на то, что в рамках сцены будет ровно один экземпляр
        INSTANCE = this;
    }

    public void PlayShootAttack()
    {
        battleSound.PlayNamedAudioClip("Shoot");
    }
    
    public void PlayMeleeAttack()
    {
        battleSound.PlayNamedAudioClip("Hit");
    }

    public void PlayStepMovementLoop()
    {
        battleSound.PlayNamedAudioClip("Step", true);
    }
    
    public void StopStepMovementLoop()
    {
        battleSound.StopCurrentAudioClip();
    }
    
    public void PlayTakeDamage()
    {
        StartCoroutine(healthSound.PlayEffectWithDelay("TakeDamage", 0.4f));
    }
    
    public void PlayDie()
    {
        StartCoroutine(healthSound.PlayEffectWithDelay("Death", 0.4f));
    }

    public void PlayWonGame()
    {
        uiSound.PlayNamedAudioClip("Win");
    }
    
    public void PlayLostGame()
    {
        uiSound.PlayNamedAudioClip("Lose");
    }

    
}
