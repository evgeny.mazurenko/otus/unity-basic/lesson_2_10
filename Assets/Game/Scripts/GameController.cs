using System;
using System.Collections;
using System.Linq;
using Game.Scripts.UI;
using Game.Scripts.Weapon;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] 
    private float turnDelay = 1f;
    
    [SerializeField] 
    private Character[] players;

    [SerializeField]
    private Character[] enemies;

    [SerializeField]
    private Weapon sniperRifle;
    
    [SerializeField]
    private UIController uiController;

    [SerializeField]
    private PostProcessVolume postProcessing;

    [SerializeField]
    private Music music;

    private ChromaticAberration _chromaticAberrationPostProcessing;

    private SoundEffectController _soundEffects;

    private Queue _turns = new Queue();

    private bool _isSelectedEnemyConfirmed;

    private bool _isLatestEnemy;

    private bool _isPaused;

    private Character _selectedEnemy;
    
    private void OnEnable()
    {
        music.PlayMusic();
    }

    private void OnDisable()
    {
        music.StopMusic();
    }
    
    void Start()
    {
        _chromaticAberrationPostProcessing = postProcessing.profile.GetSetting<ChromaticAberration>();
        _soundEffects = SoundEffectController.INSTANCE;
        
        uiController.ShowGameControls();
        
        foreach (var player in players)
        {
            _turns.Enqueue(player);
        }
        
        foreach (var enemy in enemies)
        {
            _turns.Enqueue(enemy);
        }
        StartCoroutine(MainLoop());
    }

    private IEnumerator MainLoop()
    {
        while (true)
        {
            foreach (var turn in GetNextTurn())
            {
                yield return new WaitWhile(() => _isPaused);
                if (turn is Character character && character.IsAlive)
                {
                    Character opponent;
                    if (players.Contains(character))
                    {
                        SelectRandomEnemy();
                        if (!_isLatestEnemy)
                        {
                            _isSelectedEnemyConfirmed = false;
                            yield return new WaitUntil(() => _isSelectedEnemyConfirmed);
                        }
                        opponent = _selectedEnemy;
                    }
                    else
                    {
                        opponent = GetAnyAliveTarget(players);
                    }

                    if (opponent != null)
                    {
                        yield return character.Attack(opponent);
                    }

                    _turns.Enqueue(character);
                } 
                else if (turn is Weapon weapon)
                {
                    var opponent = GetAnyAliveTarget(enemies);
                    if (opponent != null)
                    {
                        _soundEffects.PlayShootAttack();
                        opponent.TakeDamage(weapon.Damage);
                        uiController.UnlockCallSniperButton();
                        Debug.Log($"Weapon {weapon.Type} attack {opponent.name}");
                    }
                }

                yield return new WaitForSeconds(turnDelay);

                if (AreAllDead(players))
                {
                    GameLost();
                    yield break;
                }
                else if (AreAllDead(enemies))
                {
                    GameWon();
                    yield break;
                }
            }
        }
    }

    private IEnumerable GetNextTurn()
    {
        while (true)
        {
            yield return _turns.Dequeue();
        }
    }

    private void SetActivePostProcessing(bool enable)
    {
        _chromaticAberrationPostProcessing.intensity.Override(enable ? 1f : 0f);
        _chromaticAberrationPostProcessing.enabled.Override(enable);
    }

    #region Character array methods

    private bool HasAnyAlive(Character[] characters)
    {
        return characters.Any((character) => character.IsAlive);
    }

    private bool AreAllDead(Character[] characters)
    {
        return !HasAnyAlive(characters);
    }
    
    private Character GetAnyAliveTarget(Character[] characters)
    {
        return characters.First((character) => character.IsAlive);
    }

    #endregion
    

    #region EndGame

    private void GameWon()
    {
        uiController.ShowGameWonLabel();
        music.StopMusic();
        _soundEffects.PlayWonGame();
    }
    
    private void GameLost()
    {
        uiController.ShowGameLostLabel();
        music.StopMusic();
        _soundEffects.PlayLostGame();
    }

    #endregion

    #region Button Callback

    public void CallSniper()
    {
        if (!_turns.Contains(sniperRifle))
        {
            _turns.Enqueue(sniperRifle);
            uiController.LockCallSniperButton();
        }
    }

    public void ConfirmTarget()
    {
        _isSelectedEnemyConfirmed = true;
    }

    public void SelectRandomEnemy()
    {
        var nextEnemies = enemies.Where(character => character.IsAlive);
        if (nextEnemies.Count() == 1)
        {
            _isLatestEnemy = true;
            _selectedEnemy = nextEnemies.First();
            uiController.TurnControlsOff();
        }
        else if (nextEnemies.Any())
        {
            nextEnemies = nextEnemies.Where(character => !character.Equals(_selectedEnemy));
            if (nextEnemies.Any())
            {
                _selectedEnemy = nextEnemies.ToArray()[Random.Range(0, nextEnemies.Count())];
            }
        }
    }

    public void ShowGameMenu()
    {
        _isPaused = true;
        uiController.ShowMenu();
        SetActivePostProcessing(true);
    }

    public void Continue()
    {
        _isPaused = false;
        uiController.Continue();
        SetActivePostProcessing(false);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ShowMainMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    #endregion
    
}
