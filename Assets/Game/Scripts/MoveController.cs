using UnityEngine;

namespace Game.Scripts
{
    public sealed class MoveController : MonoBehaviour
    {
        [SerializeField]
        private Player player;
        
        private Transform mainCameraTransform;

        private void Start()
        {
            mainCameraTransform = Camera.main.transform;
        }
        
        private void Update()
        {
            RelativeCameraMoving();
        }

        //Определение вектора перемещения относительно позиции камеры
        private void RelativeCameraMoving()
        {
            //A-D, Left-Right
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            //W-S, Up-Down
            float verticalInput = Input.GetAxisRaw("Vertical");
            
            //cameraForward - нормализованный вектор в мировых координатах для перемещения объекта относительно позиции камеры вперед
            //Компонента Y обнуляется, так как перемещение осуществляется только в плоскости XOZ
            Vector3 cameraForward = Vector3.Scale(mainCameraTransform.forward, new Vector3(1, 0, 1)).normalized;
            
            //Для вектора mainCameraTransform.right не требуется обнулять компоненту Y и нормализовывать вектор,
            //так как камера горизонтальна и по направлению X наклон для Y отсуствует 
            Vector3 cameraRight = mainCameraTransform.right;
            
            Vector3 moveDirection = (cameraForward * verticalInput + cameraRight * horizontalInput).normalized;
            this.player.Move(moveDirection);
        }

        //Перемещение по абсолютным векторам
        private void KeyBoardMoving()
        {
            if (Input.GetKey(KeyCode.W))
            {
                this.player.Move(Vector3.forward);
                return;
            }
        
            if (Input.GetKey(KeyCode.S))
            {
                this.player.Move(Vector3.back);
                return;
            }
        
            if (Input.GetKey(KeyCode.A))
            {
                this.player.Move(Vector3.left);
                return;
            }
        
            if (Input.GetKey(KeyCode.D))
            {
                this.player.Move(Vector3.right);
                return;
            }
        }
    }
}