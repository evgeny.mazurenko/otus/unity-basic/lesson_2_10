using System;
using Game.Scripts.Weapon;
using UnityEngine;

namespace Game.Scripts
{
    public static class AnimationIdForWeaponType
    {
        public static int GetAnimationId(WeaponType weaponType)
        {
            switch (weaponType)
            {
                case WeaponType.GUN:
                    return Animator.StringToHash("Shoot");
                case WeaponType.BAT:
                    return Animator.StringToHash("Strike");
            }
            throw new ArgumentException($"Not found animation for weapon type [{weaponType}]");
        }
    }
}