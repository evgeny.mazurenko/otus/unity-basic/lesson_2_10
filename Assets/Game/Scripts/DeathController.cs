using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class DeathController : MonoBehaviour
{
    [SerializeField]
    private Player player;

    [SerializeField] 
    private int countAttackBeforeDeath = 3;

    // Update is called once per frame
    void Update()
    {
        if (player.AttackCount >= countAttackBeforeDeath)
        {
            player.Die();
        }
    }
}
