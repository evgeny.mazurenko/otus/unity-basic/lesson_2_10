using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class AttackController : MonoBehaviour
{
    [SerializeField]
    private Player player;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            player.Attack();
        }
    }
}
